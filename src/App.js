import React from 'react';
import { SafeAreaView, View, StyleSheet } from 'react-native'; 

import LogoImage  from './components/logo/LogoImage'
import ViewButtom from './components/viewButton/ViewButton'
import TextTitle  from './components/title/TextTitle'

const logoUri = './assets/logo.png';

export default () => {

    return (
        <SafeAreaView style = { style.App } >

            <View style = { style.View }>

                <LogoImage />

                <TextTitle />
                
                <ViewButtom />

            </View>

        </SafeAreaView>
)};

const style = StyleSheet.create ({
    App:{
        backgroundColor: '#fff',
        flexGrow: 1,
        alignItems: "center",
        flexDirection: "column",
    },

    View:{
        marginTop: 25,
        paddingTop: 25,
        height: "80%",
        alignItems: "center",
        width: "100%",
    },
    
});