import React from 'react';
import { StyleSheet } from 'react-native'; 

export default StyleSheet.create ({
          
    TextTitle: {
        fontWeight: "bold",
        fontSize: 22,
        marginTop: 20,
    },  
}); 