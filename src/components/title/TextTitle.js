import React, { useState } from 'react';
import { Text } from 'react-native'; 
import Style from './StyleTextTitle'

export default () => {
    return (
        <Text style = { Style.TextTitle } >
            React Native
        </Text>
    );
}