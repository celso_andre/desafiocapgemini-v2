import React, { useState } from 'react';
import {Button, SafeAreaView, StyleSheet , Image, Text, View } from 'react-native'; 
import Style from './StyleLogoImage'

export default () => {
    const logoUri = '../../assets/logo.png';
    return (
        <Image
            accessibilityLabel = "React Logo" 
            source = {require(logoUri)} style = {Style.ImageLogo}
            resizeMode = "contain"
        />
    );
}