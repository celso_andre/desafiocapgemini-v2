import React, { useState } from 'react';
import { Button, View, Text } from 'react-native'; 
import Style from './StyleViewButton'

export default () => {

    const [count, setCount] = useState(0);

    return (
        <View style ={ Style.View }>

            <Text style = { Style.TextCount } > 
                Count: { count } 
            </Text>

            <Button
                style = { Style.Butom }
                title = 'Counter Button'
                onPress = { () => setCount(count + 1) }
            />
        </View>
    );
}